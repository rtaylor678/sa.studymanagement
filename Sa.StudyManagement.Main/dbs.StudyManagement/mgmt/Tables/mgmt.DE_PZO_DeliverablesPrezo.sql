﻿CREATE TABLE [mgmt].[DE_PZO_DeliverablesPrezo]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[DeliverableId]			INT,

	[PlantId]				INT,
	[PlantNmenomic]			INT,
	[PlantGrouping]			INT,
	[SortKey]				INT,

	[DeliverablesGroups]	INT,

	[DeliverablesRank]		INT,
	[DeliverablesGap]		INT
);