﻿CREATE TABLE [mgmt].[DE_PLT_DeliverablesPlant]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[DeliverableId]			INT,

	[PlantId]				INT,

	[InputForm]				INT,
	[InputValidated]		INT,
	[CT]					INT,
	[Trends]				INT
);