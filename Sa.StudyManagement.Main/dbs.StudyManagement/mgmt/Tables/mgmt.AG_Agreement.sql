﻿CREATE TABLE [mgmt].[AG_Agreement]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[CompanyId]				INT,
	[PlantId]				INT,
	[GroupId]				INT,
	[Deliverables]			INT
);