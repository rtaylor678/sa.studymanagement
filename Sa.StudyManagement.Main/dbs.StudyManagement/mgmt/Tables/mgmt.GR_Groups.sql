﻿CREATE TABLE [mgmt].[GR_Groups]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[GroupId]		INT,
	[PlantId]		INT
);