﻿CREATE TABLE [mgmt].[DE_OUT_DeliverablesOutput]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[DeliverableId]			INT,

	[GroupId]				INT,
	[SortKey]				INT
);