﻿CREATE TABLE [posit].[PO_Positor]
(
	[PO_PositorId]			INT					NOT	NULL	IDENTITY(1, 1),

	[PO_PositorApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_PositorApp]		DEFAULT(APP_NAME()),
															CONSTRAINT [CL__PO_Positor_PO_PositorApp]		CHECK([PO_PositorApp] <> ''),
	[PO_PositorName]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_PositorName]		DEFAULT(SUSER_SNAME()),
															CONSTRAINT [CL__PO_Positor_PO_PositorName]		CHECK([PO_PositorName] <> ''),
	[PO_PositorHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_PositorHost]		DEFAULT(HOST_NAME()),
															CONSTRAINT [CL__PO_Positor_PO_PositorHost]		CHECK([PO_PositorHost] <> ''),

	[PO_PositorSID]			VARBINARY(85)		NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_PositorSID]		DEFAULT(SUSER_SID()),
	[PO_PositorUID]			INT					NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_PositorUID]		DEFAULT(SUSER_ID()),

	[PO_PositorNote]		VARCHAR(348)			NULL	CONSTRAINT [CL__PO_Positor_PO_PositorNote]		CHECK([PO_PositorNote] <> ''),
	[PO_PositorLoginId]		INT						NULL,

	[PO_PositorInserted]	DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_PositorInserted]	DEFAULT(SYSDATETIMEOFFSET()),

	[PO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PO_Positor_PO_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__PO_Positor_PO_RowGuid]			UNIQUE NONCLUSTERED([PO_RowGuid]),

	CONSTRAINT [UK__Positor]	UNIQUE CLUSTERED ([PO_PositorApp] ASC, [PO_PositorHost] ASC, [PO_PositorUID] ASC, [PO_PositorLoginId] ASC),
	CONSTRAINT [PK__Positor]	PRIMARY KEY ([PO_PositorId] ASC)
);