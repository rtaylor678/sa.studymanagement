﻿CREATE PROCEDURE [posit].[Insert_Positor]
(
	@PositorNote	NVARCHAR(348) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(257) = OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID);
	SET @PositorNote = COALESCE(@PositorNote, N'Automatically created by: ' + @ProcedureDesc);

	DECLARE @Id TABLE
	(
		[Id]	INT		NOT	NULL
	);

	INSERT INTO [posit].[PO_Positor]([PO_PositorNote])
	OUTPUT [INSERTED].[PO_PositorId]
	INTO @Id([Id])
	VALUES(@PositorNote);

	RETURN (SELECT TOP 1 [Id] FROM @Id);

END;
GO