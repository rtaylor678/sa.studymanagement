﻿CREATE PROCEDURE [posit].[Return_PositorId]
AS
BEGIN

	DECLARE @PositorId	INT = [posit].[Get_PositorId]();

	IF (@PositorId IS NULL)
	BEGIN

		DECLARE @ProcedureDesc	NVARCHAR(257) = OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID);
		DECLARE @PositorNote	NVARCHAR(348) = N'Automatically created by: ' + @ProcedureDesc;

		EXECUTE @PositorId = [posit].[Insert_Positor] NULL, @PositorNote;

	END;

	RETURN @PositorId;

END;