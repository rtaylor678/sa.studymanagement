﻿SELECT *
FROM (VALUES
	('A', 'All'),
	('C', 'Company'),
	('D', 'Divsion'),
	('P', 'Plant')
	)[Visibility]([Tag], [Name]);

SELECT *
FROM (VALUES
	--	Database
	('C', 'Create'),
	('R', 'Read'),
	('U', 'Update'),
	('D', 'Delete'),
	('X', 'Execute'),

	--	File
	('C', 'Create'),
	('R', 'Read'),
	('U', 'Update'),
	('D', 'Delete'),

	('A', 'Archive'),	--	Move
	('Z', 'Compress'),	--	Zip

	--	Communications
	('W', 'Write'),
	('E', 'Edit'),
	('S', 'Send'),
	('R', 'Receive')
	
	)[Permissions]([Tag], [Name]);

DECLARE @Permissions TABLE
(
	[init]	INT			NOT NULL,

	[C]		INT			NOT	NULL	DEFAULT(1),
	[R]		INT			NOT	NULL	DEFAULT(1),
	[U]		INT			NOT	NULL	DEFAULT(1),
	[D]		INT			NOT	NULL	DEFAULT(1),

	[A]		INT			NOT	NULL	DEFAULT(1),
	[Z]		INT			NOT	NULL	DEFAULT(1),

	[W]		INT			NOT	NULL	DEFAULT(1),
	[E]		INT			NOT	NULL	DEFAULT(1),
	[S]		INT			NOT	NULL	DEFAULT(1),

	[cs]	AS CHECKSUM([C], [R], [U], [D], [A], [Z], [W], [E], [S])
);

INSERT INTO @Permissions([init]) VALUES(0), (1), (2);

SELECT * FROM @Permissions;

UPDATE @Permissions SET [C] = 0, [R] = 0
WHERE init = 2

SELECT * FROM @Permissions;

DECLARE @RWX TABLE
(
	[R]		INT			NOT	NULL	DEFAULT(1),
	[W]		INT			NOT	NULL	DEFAULT(1),
	[X]		INT			NOT	NULL	DEFAULT(1),

	[RWX]	AS	CONVERT(CHAR(3),
					CONVERT(CHAR(1), [R]) +
					CONVERT(CHAR(1), [W]) +
					CONVERT(CHAR(1), [X])
					)
);

INSERT INTO @RWX([R], [W], [X])
VALUES
	(1, 1, 1),	--	7
	(1, 1, 0),	--	6
	(1, 0, 1),	--	5
	(1, 0, 0),	--	4
	(0, 1, 1),	--	3
	(0, 1, 0),	--	2
	(0, 0, 1),	--	1
	(0, 0, 0);	--	0

SELECT * FROM @RWX;